package com.malcolm.readr;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by malcolm on 6/17/13.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    //DataBase Version
    public static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "ReviewList";

    // Table name(s)
    private static final String TABLE_BOOK_REVIEWS = "bookReviews";

    // Book Reviews Table Column Names
    private static final String BOOK_REVIEW_ID = "bookReviewID";
    private static final String BOOK_ID = "BookId";
    private static final String BOOK_TITLE = "Title";
    private static final String BOOK_AUTHOR = "Author";
    private static final String BOOK_PUBLISHER = "Publisher";
    private static final String BOOK_REVIEWER = "Reviewer";
    private static final String BOOK_REVIEW_DATE = "ReviewDate";
    private static final String BOOK_BRIEF = "Brief";
    private static final String BOOK_REVIEW_URL = "ReviewUrl";
    private static final String BOOK_ISBN = "ISBN";
    private static final String BOOK_LINK = "BookLink";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    //Create Table(s)
    @Override
    public void onCreate(SQLiteDatabase db){
        //Create Book Reviews Table
        String CREATE_BOOK_REVIEWS_TABLE = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY, " +
                "%s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s, TEXT, %s TEXT, " +
                "%s TEXT, %s TEXT);",
                TABLE_BOOK_REVIEWS, BOOK_REVIEW_ID, BOOK_ID, BOOK_TITLE, BOOK_AUTHOR, BOOK_PUBLISHER,
                BOOK_REVIEWER, BOOK_REVIEW_DATE, BOOK_BRIEF, BOOK_REVIEW_URL, BOOK_ISBN, BOOK_LINK);
        db.execSQL(CREATE_BOOK_REVIEWS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOK_REVIEWS);
        // Create table(s) again
        onCreate(db);
    }

    public void recreateBookReviewsTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOK_REVIEWS);

        //recreate
        String CREATE_BOOK_REVIEWS_TABLE = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY, " +
                "%s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s, TEXT, %s TEXT, " +
                "%s TEXT, %s TEXT);",
                TABLE_BOOK_REVIEWS, BOOK_REVIEW_ID, BOOK_ID, BOOK_TITLE, BOOK_AUTHOR, BOOK_PUBLISHER,
                BOOK_REVIEWER, BOOK_REVIEW_DATE, BOOK_BRIEF, BOOK_REVIEW_URL, BOOK_ISBN, BOOK_LINK);
        db.execSQL(CREATE_BOOK_REVIEWS_TABLE);
    }

    //Insert new Book Review record
    public void insertBookReview(String bookID, String title, String author, String publisher,
                                 String reviewer, String reviewDate, String brief, String reviewUrl,
                                 String isbn, String bookLink){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(BOOK_ID, bookID);
        values.put(BOOK_TITLE, title);
        values.put(BOOK_AUTHOR, author);
        values.put(BOOK_PUBLISHER, publisher);
        values.put(BOOK_REVIEWER, reviewer);
        values.put(BOOK_REVIEW_DATE, reviewDate);
        values.put(BOOK_BRIEF, brief);
        values.put(BOOK_REVIEW_URL, reviewUrl);
        values.put(BOOK_ISBN, isbn);
        values.put(BOOK_LINK, bookLink);

        db.insert(TABLE_BOOK_REVIEWS, null, values);
        db.close();
    }

    public List<String> getBookTitles(){
        List<String> titles = new ArrayList<String>();

        String sql = "SELECT " + BOOK_TITLE + " FROM " + TABLE_BOOK_REVIEWS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);

        if (c.moveToFirst()){
            do {
                titles.add(c.getString(0));
            } while (c.moveToNext());
        }
        c.close();
        return titles;
    }


    public List<String> getBookAuthors(){
        List<String> authors = new ArrayList<String>();

        String sql = "SELECT " + BOOK_AUTHOR + " FROM " + TABLE_BOOK_REVIEWS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);

        if (c.moveToFirst()){
            do {
                authors.add(c.getString(0));
            } while (c.moveToNext());
        }
        c.close();
        return authors;
    }

    public List<String> getBookPublishers(){
        List<String> publishers = new ArrayList<String>();

        String sql = "SELECT " + BOOK_PUBLISHER + " FROM " + TABLE_BOOK_REVIEWS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);

        if (c.moveToFirst()){
            do {
                publishers.add(c.getString(0));
            } while (c.moveToNext());
        }
        c.close();
        return publishers;
    }

    public List<String> getBookReviewers(){
        List<String> reviewers = new ArrayList<String>();

        String sql = "SELECT " + BOOK_REVIEWER + " FROM " + TABLE_BOOK_REVIEWS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);

        if (c.moveToFirst()){
            do {
                reviewers.add(c.getString(0));
            } while (c.moveToNext());
        }
        c.close();
        return reviewers;
    }

    public List<String> getBookReviewDates(){
        List<String> dates = new ArrayList<String>();

        String sql = "SELECT " + BOOK_REVIEW_DATE + " FROM " + TABLE_BOOK_REVIEWS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);

        if (c.moveToFirst()){
            do {
                dates.add(c.getString(0));
            } while (c.moveToNext());
        }
        c.close();
        return dates;
    }

    public List<String> getBookReviewUrls(){
        List<String> urls = new ArrayList<String>();

        String sql = "SELECT " + BOOK_REVIEW_URL + " FROM " + TABLE_BOOK_REVIEWS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);

        if (c.moveToFirst()){
            do {
                urls.add(c.getString(0));
            } while (c.moveToNext());
        }
        c.close();
        return urls;
    }

    public List<String> getBookBriefs(){
        List<String> briefs = new ArrayList<String>();

        String sql = "SELECT " + BOOK_BRIEF + " FROM " + TABLE_BOOK_REVIEWS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);

        if (c.moveToFirst()){
            do {
                briefs.add(c.getString(0));
            } while (c.moveToNext());
        }
        c.close();
        return briefs;
    }


}
