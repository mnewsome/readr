package com.malcolm.readr;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.support.v4.app.ListFragment;

import java.util.List;




/**
 * Created by malcolm on 6/16/13.
 */
public class BookList extends ListFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        DatabaseHandler db = new DatabaseHandler(getActivity());
        //Get List of titles
        List<String> bookTitles = db.getBookTitles();
        ///Populate using adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(inflater.getContext(),
                android.R.layout.simple_list_item_1,bookTitles);
        // Set the list adapter
        setListAdapter(adapter);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    //Create callback so data can be passed to activity
    OnBookSelectedListener bCallback;

    public interface OnBookSelectedListener {
        public void onBookSelected(int position);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            bCallback = (OnBookSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnBookSelectedListener");
        }
    }

    @Override
    public void onListItemClick(ListView listview, View v, int position, long id) {
        bCallback.onBookSelected(position);
    }
}


