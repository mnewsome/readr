package com.malcolm.readr;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.support.v4.app.Fragment;

import java.util.List;


/**
 * Created by malcolm on 6/16/13.
 */
public class BookView extends Fragment {


    final static String ARG_POSITION = "position";
    int currentPosition = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View root = new View(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                                        ViewGroup.LayoutParams.MATCH_PARENT));
        root.setBackgroundColor(Color.CYAN);

        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.fragment_bookview, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {
            updateBookDetails(args.getInt(ARG_POSITION));
        } else if (currentPosition != -1) {
            updateBookDetails(currentPosition);
        }
    }

    public void updateBookDetails(int position) {
        DatabaseHandler db = new DatabaseHandler(getActivity());

        //Import records into Lists to prevent repeat calls to the database.
        List<String> titles = db.getBookTitles();
        ArrayAdapter<String> titleAdapter = new ArrayAdapter<String>(getActivity(),
                                                                    R.id.txt_title, titles);

        List<String> authors = db.getBookAuthors();
        ArrayAdapter<String> authorAdapter = new ArrayAdapter<String>(getActivity(),
                                                                    R.id.txt_author, authors);

        List<String> publishers = db.getBookPublishers();
        ArrayAdapter<String> publisherAdapter = new ArrayAdapter<String>(getActivity(),
                                                                    R.id.txt_publisher, publishers);

        List<String> reviewers = db.getBookReviewers();
        ArrayAdapter<String> reviewerAdapter = new ArrayAdapter<String>(getActivity(),
                                                                    R.id.txt_reviewer, reviewers);

        List<String> reviewDates = db.getBookReviewDates();
        ArrayAdapter<String> reviewDateAdapter = new ArrayAdapter<String>(getActivity(),
                                                                    R.id.txt_reviewDate, reviewDates);

        List<String> reviewUrls = db.getBookReviewUrls();
        ArrayAdapter<String> reviewUrlAdapter = new ArrayAdapter<String>(getActivity(),
                                                                    R.id.txt_reviewUrl, reviewUrls);

        List<String> briefs = db.getBookBriefs();
        ArrayAdapter<String> briefAdapter = new ArrayAdapter<String>(getActivity(),
                                                                    R.id.txt_brief, briefs);


        //Set TextViews
        TextView bookTitle = (TextView) getActivity().findViewById(R.id.txt_title);
        bookTitle.setText(titleAdapter.getItem(position));

        TextView bookAuthor = (TextView) getActivity().findViewById(R.id.txt_author);
        bookAuthor.setText("Author: " + authorAdapter.getItem(position));

        TextView bookPublisher = (TextView) getActivity().findViewById(R.id.txt_publisher);
        bookPublisher.setText("Publisher: " + publisherAdapter.getItem(position));

        TextView bookReviewer = (TextView) getActivity().findViewById(R.id.txt_reviewer);
        bookReviewer.setText("Reviewed by: " + reviewerAdapter.getItem(position));

        TextView bookReviewDate = (TextView) getActivity().findViewById(R.id.txt_reviewDate);
        String dateString = reviewDateAdapter.getItem(position);
        bookReviewDate.setText("Review Date: " + dateString.substring(0, 9));

        TextView bookReviewUrl = (TextView) getActivity().findViewById(R.id.txt_reviewUrl);
        bookReviewUrl.setClickable(true);
        bookReviewUrl.setMovementMethod(LinkMovementMethod.getInstance());
        String hyperlink = "<a href='" + reviewDateAdapter.getItem(position) + "'>Read Entire Review </a>";
        bookReviewUrl.setText(Html.fromHtml(hyperlink));

        TextView bookBrief = (TextView) getActivity().findViewById(R.id.txt_brief);
        String text = briefAdapter.getItem(position);
        bookBrief.setText("Brief: " + text.substring(3, text.lastIndexOf("<")));

        currentPosition = position;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save the current article selection in case we need to recreate the fragment
        outState.putInt(ARG_POSITION, currentPosition);
    }

}

