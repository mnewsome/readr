package com.malcolm.readr;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SlidingPaneLayout;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


public class MainActivity extends FragmentActivity implements BookList.OnBookSelectedListener {

    // Book Reviews Table Column Names
    private static final String BOOK_REVIEW_ID = "bookReviewID";
    private static final String BOOK_ID = "BookId";
    private static final String BOOK_TITLE = "Title";
    private static final String BOOK_AUTHOR = "Author";
    private static final String BOOK_PUBLISHER = "Publisher";
    private static final String BOOK_REVIEWER = "Reviewer";
    private static final String BOOK_REVIEW_DATE = "ReviewDate";
    private static final String BOOK_BRIEF = "Brief";
    private static final String BOOK_REVIEW_URL = "ReviewUrl";
    private static final String BOOK_ISBN = "ISBN";
    private static final String BOOK_LINK = "BookLink";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String url = "http://api.usatoday.com/open/reviews/books/recent?count=10&api_key=ch39p2zhfbvztz59mdfvtry6";
        //Call AsyncTask to get data from API
        new LoadBooks().execute(url);

    }


    //Method to make HTTP Request
    public String getApiData(String URL) {
        StringBuilder stringBuilder = new StringBuilder();
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(URL);
        try {
            HttpResponse response = httpClient.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream inputStream = entity.getContent();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                inputStream.close();
            } else {
                Log.d("JSON", "Failed to download file");
            }
        } catch (Exception e) {
            Log.d("getApiData", e.getLocalizedMessage());
        }
        return stringBuilder.toString();
    }

    @Override
    public void onBookSelected(int position) {

        BookView bookDetails = (BookView)
                getSupportFragmentManager().findFragmentById(R.id.fragment_bookDetails);

        if (bookDetails != null) {
            bookDetails.updateBookDetails(position);
        } else {

        }
    }

    //Process in background
    class LoadBooks extends AsyncTask<String, Void, String> {
        protected String doInBackground(String...url) {
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());

            String bookID;
            String title;
            String author;
            String publisher;
            String reviewer;
            String reviewDate;
            String brief;
            String reviewUrl;
            String isbn;
            String bookLink;


            String result = getApiData(url[0]);

            //prevent duplicates
            db.recreateBookReviewsTable();

            try {
                JSONObject jObject = new JSONObject(result);
                JSONArray books = new JSONArray(jObject.getString("BookReviews"));

                for(int i = 0; i < books.length(); i++){
                    JSONObject b = books.getJSONObject(i);

                    try {
                    //store values
                    bookID = b.getString(BOOK_ID);
                    title = b.getString(BOOK_TITLE);
                    author = b.getString(BOOK_AUTHOR);
                    publisher = b.getString(BOOK_PUBLISHER);
                    reviewer = b.getString(BOOK_REVIEWER);
                    reviewDate = b.getString(BOOK_REVIEW_DATE);
                    brief = b.getString(BOOK_BRIEF);
                    reviewUrl = b.getString(BOOK_REVIEW_URL);
                    isbn = b.getString(BOOK_ISBN);
                    bookLink = b.getString(BOOK_LINK);

                    db.insertBookReview(bookID, title, author, publisher,reviewer, reviewDate,
                            brief, reviewUrl,isbn, bookLink);

                    } catch (Exception e) {
                        Log.d("JSON Values", e.getLocalizedMessage());
                    }
                }
            } catch (Exception e ){
                Log.d("Load Books AsyncTask", e.getLocalizedMessage());

            }

            return "Data Loaded";

        }

        protected void onPostExecute(String result) {

            final View root = getLayoutInflater().inflate(R.layout.activity_main, null);
            setContentView(root);

            final SlidingPaneLayout slidingPaneLayout =
                    SlidingPaneLayout.class.cast(root.findViewById(R.id.slidingpanelayout));
            slidingPaneLayout.setPanelSlideListener(new SlidingPaneLayout.PanelSlideListener() {

                @Override
                public void onPanelSlide(View view, float v) {
                    TextView welcomeMsg = (TextView)findViewById(R.id.txt_welcome);
                    welcomeMsg.setText("");
                }

                @Override
                public void onPanelOpened(View view) {
                    switch (view.getId()) {
                        case R.id.fragment_bookDetails:
                            getActionBar().setTitle("ReadR -- Book Review List");

                            break;
                        default:
                            break;
                    }
                }

                @Override
                public void onPanelClosed(View view) {
                    switch (view.getId()) {
                        case R.id.fragment_bookDetails:
                            getActionBar().setTitle("ReadR -- Book Review Details");
                            break;
                        default:
                            break;
                    }
                }
            });
        }
    }

}
